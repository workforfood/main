<?php

session_start();

if(!isset($_SESSION['login'])) {
	header("Location: auth.php");
}
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Main</title>
	<link rel="shortcut icon" href="/frontend/favicon/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="/frontend/css/bootstrap.min.css">
	<link rel="stylesheet" href="/frontend/css/style.css">
</head>
<body>
<p>Ник: <?php echo $_SESSION['login'] ?></p>
<p><a href="logout.php">Выйти</a></p>
</body>
</html>