<?php

session_start();

require 'core/auth.class.php';

if($_SERVER['REQUEST_METHOD'] == 'POST') {
	$login = $_POST['login'];
	$pass = $_POST['pass'];
	$test = new auth($login, $pass);
}

?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Authorization</title>
	<link rel="shortcut icon" href="/frontend/favicon/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="/frontend/css/bootstrap.min.css">
	<link rel="stylesheet" href="/frontend/css/style.css">
</head>
<body>
	<div class="auth">
		<form id="login" class="login" method="post" action="<?php echo $_SERVER['SCRIPT_NAME'];?>">
			<div class="form-row">
				<div class="form-header">Authorization</div>
				<div class="form-group"><p class="error"><?php echo $test->result; ?></p></div>
				<div class="form-group">
					<label for="login">Login</label>
					<input type="text" class="form-control" id="login" name="login" placeholder="Login" required>
				</div>
				<div class="form-group">
					<label for="pass">Password</label>
					<input type="password" class="form-control" id="pass" name="pass" placeholder="Password" required>
				</div>
				<button type="submit" class="btn btn-primary">Sign in</button>
			</div>
		</form>
	</div>
</body>
</html>