<?php

class auth {
	protected $login;
	protected $pass;
	protected $users = 'core/users.json';
	protected $result;

	public function __construct($login, $pass) {
		$this->login = $login;
		$this->pass = $pass;
		$this->result = $this->validate();

		if($this->result === true) {
			$_SESSION['login'] = $this->login;
			header("Location: index.php");
		}else{
			header("Location: auth.php");
		}
	}

	protected function validate() {
		$data = json_decode(file_get_contents($this->users), true);
		if(array_search($this->login, array_column($data, 'login')) !== false) {
			for($i = 0; $i < count($data); $i++) {
				if(array_search($this->login, $data[$i])) {
					$baseLogin = $data[$i]['login'];
					$basePass = $data[$i]['pass'];
					if($basePass == $this->pass) {
						return true;
					}else{
						return "Пароль неверный";
					}
				}
			}
		}else{
			return "Такого логина нет";
		}
	}
}
 ?>